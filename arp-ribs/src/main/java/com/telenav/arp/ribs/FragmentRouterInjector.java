package com.telenav.arp.ribs;

public interface FragmentRouterInjector<FR extends FragmentRouter> {
    /**
     * Inject the fragment.
     *
     * @param fragmentRouter to inject.
     */
    void inject(FR  fragmentRouter);
}