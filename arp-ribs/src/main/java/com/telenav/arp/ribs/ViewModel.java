package com.telenav.arp.ribs;

import android.support.annotation.NonNull;

import com.uber.rib.core.Interactor;

/**
 * View model manages view bindings and
 * interacts with other view models via interactor
 * @param <I> specific interactor type
 */
public abstract class ViewModel<I extends Interactor> {
    private final I interactor;

    public ViewModel(I interactor) {
        this.interactor = interactor;
    }

    /**
     * Return instance of flow that owns view model
     * @return
     */
    public I getInteractor() {
        return interactor;
    }

    /**
     * Returns view model tag
     * @return tag
     */
    @NonNull
    public abstract String getTag();

    /**
     * Injector for view model
     * @param <VM>
     */
    public interface ViewModelInjector<VM> {
        /**
         * Inject instance of view model
         * @param viewModel instance
         */
        void inject(VM viewModel);
    }
}