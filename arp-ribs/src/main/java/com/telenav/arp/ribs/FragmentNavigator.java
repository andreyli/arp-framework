package com.telenav.arp.ribs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.telenav.arp.ribs.utils.ActivityUtils;

class FragmentNavigator implements Navigator<Fragment> {
    private final FragmentManager fragmentManager;
    private final int containerId;

    public FragmentNavigator(FragmentManager fragmentManager, int containerId) {
        this.fragmentManager = fragmentManager;
        this.containerId = containerId;
    }

    @Override
    public Fragment findView(Class<? extends Fragment> clazz) {
        return fragmentManager.findFragmentByTag(clazz.getName());
    }

    @Override
    public void goTo(Fragment fragment) {
        ActivityUtils.replaceFragment(fragmentManager, fragment, containerId, fragment.getClass().getName(), true);
    }

    @Override
    public void replaceWith(Fragment fragment) {
        ActivityUtils.replaceFragment(fragmentManager, fragment, containerId, fragment.getClass().getName(), false);
    }

    @Override
    public boolean goBack() {
        return fragmentManager.popBackStackImmediate();
    }

    @Override
    public void resetTo(Fragment fragment) {
        ActivityUtils.resetToFragment(fragmentManager, fragment, containerId, fragment.getClass().getName(), true);
    }
}