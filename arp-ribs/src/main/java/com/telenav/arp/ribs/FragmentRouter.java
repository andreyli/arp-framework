package com.telenav.arp.ribs;

import android.support.v4.app.Fragment;

import com.uber.rib.core.Interactor;
import com.uber.rib.core.InteractorBaseComponent;
import com.uber.rib.core.Router;

import javax.inject.Inject;

/**
 * Router subclass that has fragment.
 *
 * @param <F> type of fragment owned by the router.
 * @param <I> type of interactor owned by the router.
 * @param <C> type of dependency owned by the router.
 */
public abstract class FragmentRouter<
        F extends Fragment, I extends Interactor, C extends InteractorBaseComponent>
        extends Router<I, C> {

    @Inject Navigator navigator;

    public FragmentRouter(I interactor, C component) {
        super(interactor, component);
        // Ugly but works :(
        ((FragmentRouterInjector) component).inject(this);
    }

    /** @return the router's fragment. */
    public abstract F getFragment();

    /**
     * Return instance of navigator
     * @return navigator
     */
    public Navigator getNavigator() {
        return navigator;
    }
}