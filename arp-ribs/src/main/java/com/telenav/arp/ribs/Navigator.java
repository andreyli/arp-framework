package com.telenav.arp.ribs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Generic interface for navigation between screens,
 * based on backstack
 * @param <T> generic view or metadata object
 */
public interface Navigator<T> {
    /**
     * Returns instance of the screen in backstack
     * @param clazz
     * @return instance of screen or null if view is not in backstack
     */
    @Nullable
    T findView(Class<? extends T> clazz);

    /**
     * Navigate to specific screen and add it to backstack
     * @param view instance of screen
     */
    void goTo(@NonNull T view);

    /**
     * Navigate to specific screen and replace top of backstack with it
     * @param view instance of screen
     */
    void replaceWith(@NonNull T view);

    /**
     * Navigate to previous screen in backstack
     * @return true if there's a screen in backstack otherwise false
     */
    boolean goBack();

    /**
     * Navigate to the specific screen and remove all the screens after it in backstack
     * @param view instance of screen to reset to
     */
    void resetTo(@NonNull T view);
}
