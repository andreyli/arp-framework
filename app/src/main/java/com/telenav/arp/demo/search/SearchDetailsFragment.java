package com.telenav.arp.demo.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.telenav.arp.demo.R;

public class SearchDetailsFragment extends Fragment implements SearchInteractor.SearchPresenter {
    private SearchDetailsViewModel viewModel;

    public SearchDetailsFragment() {
        // Requires empty public constructor
    }

    public static SearchDetailsFragment newInstance() {
        return new SearchDetailsFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_details_fragment, container, false);
        Button backButton = view.findViewById(R.id.backButton);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.onBackButtonClicked();
            }
        });

        Button goButton = view.findViewById(R.id.goButton);
        goButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                /// Equivalent to start navigation
                viewModel.navigateTo("Telenav");
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
