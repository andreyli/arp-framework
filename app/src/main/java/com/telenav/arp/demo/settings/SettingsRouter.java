package com.telenav.arp.demo.settings;

import com.telenav.arp.demo.settings.SettingsBuilder.Component;
import com.telenav.arp.ribs.FragmentRouter;

import javax.inject.Inject;

import dagger.Lazy;

public class SettingsRouter extends FragmentRouter<SettingsFragment, SettingsInteractor, Component> {

    @Inject
    Lazy<SettingsFragment> settingsFragmentProvider;

    public SettingsRouter(SettingsInteractor interactor, SettingsBuilder.Component component) {
        super(interactor, component);
    }

    @Override
    public SettingsFragment getFragment() {
        return settingsFragmentProvider.get();
    }
}
