package com.telenav.arp.demo;

import android.support.annotation.Nullable;

import com.telenav.arp.demo.map.MapBuilder;
import com.telenav.arp.demo.map.MapRouter;
import com.telenav.arp.ribs.Navigator;
import com.uber.rib.core.ViewRouter;

public class RootRouter extends ViewRouter<RootView, RootInteractor, RootBuilder.Component> {
    private final MapBuilder mapBuilder;
    private final Navigator navigator;

    @Nullable private MapRouter mapRouter;

    public RootRouter(Navigator navigator,
                      RootView view,
                      RootInteractor interactor,
                      RootBuilder.Component component,
                      MapBuilder mapBuilder) {
        super(view, interactor, component);
        this.mapBuilder = mapBuilder;
        this.navigator = navigator;
    }

    void attachMap() {
        mapRouter = mapBuilder.build(navigator);
        attachChild(mapRouter);
        mapRouter.getNavigator().goTo(mapRouter.getFragment());
    }

    void attachFTUE() {
        ///TODO: TBI
    }
}
