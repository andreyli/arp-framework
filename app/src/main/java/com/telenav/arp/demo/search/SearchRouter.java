package com.telenav.arp.demo.search;

import com.telenav.arp.ribs.FragmentRouter;

import javax.inject.Inject;

import dagger.Lazy;

public class SearchRouter extends FragmentRouter<SearchFragment, SearchInteractor, SearchBuilder.Component> {

    @Inject
    Lazy<SearchFragment> searchFragmentProvider;

    public SearchRouter(SearchInteractor interactor,
                        SearchBuilder.Component component) {
        super(interactor, component);
    }

    @Override
    public SearchFragment getFragment() {
        return searchFragmentProvider.get();
    }

    void attachDetails(String address) {
    }
}
