package com.telenav.arp.demo.settings;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.telenav.arp.demo.R;
import com.telenav.arp.demo.settings.SettingsBuilder.SettingsScope;
import com.telenav.arp.ribs.ViewModel.ViewModelInjector;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

@SettingsScope
public class SettingsFragment extends DaggerFragment implements SettingsInteractor.SettingsPresenter, ViewModelInjector<SettingsViewModel> {
    private SettingsViewModel viewModel;

    @Inject
    public SettingsFragment() {
        // Requires empty public constructor
    }

    @Override
    public void inject(@NonNull SettingsViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        Button backButton = view.findViewById(R.id.backButton);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.onBackButtonClicked();
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}