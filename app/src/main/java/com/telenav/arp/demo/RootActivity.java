package com.telenav.arp.demo;

import android.view.ViewGroup;

import com.telenav.arp.ribs.DaggerRibActivity;
import com.uber.rib.core.ViewRouter;

public class RootActivity extends DaggerRibActivity {
    @Override
    protected ViewRouter<?, ?, ?> createRouter(ViewGroup parentViewGroup) {
        RootBuilder rootBuilder = new RootBuilder(new RootBuilder.ParentComponent() {});
        return rootBuilder.build(navigator(R.id.fragmentContainer), parentViewGroup);
    }
}