package com.telenav.arp.demo;

import android.support.annotation.Nullable;

import com.uber.rib.core.Bundle;
import com.uber.rib.core.Interactor;
import com.uber.rib.core.RibInteractor;

import javax.inject.Inject;

@RibInteractor
public class RootInteractor extends Interactor<RootInteractor.RootPresenter, RootRouter> {
    @Inject RootPresenter presenter;

    @Override
    protected void didBecomeActive(@Nullable Bundle savedInstanceState) {
        super.didBecomeActive(savedInstanceState);
        /// TODO: if user logged in attach map otherwise attach FTUE
        getRouter().attachMap();
    }

    /** Presenter interface implemented by this RIB's view. */
    interface RootPresenter {
    }
}