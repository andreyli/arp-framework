package com.telenav.arp.demo.map;

import android.support.annotation.NonNull;

import com.telenav.arp.ribs.ViewModel;

class MapViewModel extends ViewModel<MapInteractor> {

    public MapViewModel(MapInteractor interactor) {
        super(interactor);
    }

    @Override
    @NonNull
    public String getTag() {
        return getClass().getName();
    }

    public void handleSettingsButtonClick(String param) {
        getInteractor().attachSettings();
    }

    public void handleSearchButtonClick() {
        getInteractor().attachSearch();
    }
}
