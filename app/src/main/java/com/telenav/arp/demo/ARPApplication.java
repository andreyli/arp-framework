package com.telenav.arp.demo;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class ARPApplication extends DaggerApplication {
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerApplicationComponent.builder().application(this).build();
    }
}
