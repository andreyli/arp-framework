package com.telenav.arp.demo.denalisearch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.telenav.arp.demo.R;
import com.telenav.arp.demo.search.SearchViewModel;

public class DenaliSearchFragment extends Fragment {
    private SearchViewModel viewModel;
    private EditText searchText;

    public DenaliSearchFragment() {
        // Requires empty public constructor
    }

    public static DenaliSearchFragment newInstance() {
        return new DenaliSearchFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.denali_search_fragment, container, false);

        searchText = view.findViewById(R.id.searchEditText);

        Button backButton = view.findViewById(R.id.backButton);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.onBackButtonClicked();
            }
        });

        Button goButton = view.findViewById(R.id.goButton);
        goButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.onGoButtonClicked(searchText.getText().toString());
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}