package com.telenav.arp.demo.map;

import android.support.annotation.Nullable;

import com.telenav.arp.demo.map.MapInteractor.MapPresenter;
import com.telenav.arp.demo.search.SearchInteractor;
import com.telenav.arp.demo.settings.SettingsInteractor;
import com.uber.rib.core.Bundle;
import com.uber.rib.core.Interactor;
import com.uber.rib.core.RibInteractor;

import javax.inject.Inject;

@RibInteractor
public class MapInteractor extends Interactor<MapPresenter, MapRouter> {
    @Inject MapPresenter presenter;
    @Inject MapViewModel viewModel;

    @Override
    protected void didBecomeActive(@Nullable Bundle savedInstanceState) {
        super.didBecomeActive(savedInstanceState);
        /// View model needs to be injected before fragment is inflated
        getRouter().getFragment().inject(viewModel);
    }

    void attachSearch() {
        getRouter().attachSearch();
    }

    void attachSettings() {
        getRouter().attachSettings();
    }

    /** Presenter interface implemented by this RIB's view. */
    interface MapPresenter {
    }

    class SearchListener implements SearchInteractor.Listener {
        @Override
        public void close() {
            getRouter().detachSearch();
        }
    }

    class SettingsListener implements SettingsInteractor.Listener {
        @Override
        public void close() {
            getRouter().detachSettings();
        }
    }
}

/*
class SearchHandler implements SearchViewModel.Listener {
    @Override
    public void close() {
        getRouter().detachSearch();
    }
}

class SettingsHandler implements SettingsViewModel.Listener {
    @Override
    public void close() {
        getRouter().detachSettings();
    }
}

class SearchDetailsHandler implements SearchDetailsViewModel.Listener {
    @Override
    public void navigateTo(String destination) {
        getRouter().detachSearch();
        getRouter().attachNavigation(destination);
    }
} */