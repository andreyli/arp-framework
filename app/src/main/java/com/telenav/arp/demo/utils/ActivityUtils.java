package com.telenav.arp.demo.utils;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class ActivityUtils {

    private ActivityUtils() {
    }

    /**
     * The {@code fragment} is added to the container view with id {@code frameId}. The operation is
     * performed by the {@code fragmentManager}.
     *
     */
    public static void addFragmentToActivity (@NonNull FragmentManager fragmentManager,
                                              @NonNull Fragment fragment, int frameId) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment);
        transaction.commit();
    }

    /**
     * The {@code fragment} is added to the container view with id {@code frameId}. The operation is
     * performed by the {@code fragmentManager}.
     *
     */
    public static void addFragmentToActivity (@NonNull FragmentManager fragmentManager,
                                              @NonNull Fragment fragment, String tag, boolean backstack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(fragment, tag);
        if (backstack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
        fragmentManager.executePendingTransactions();
    }

    /**
     * The {@code fragment} is replaced in the container view with id {@code frameId}.
     * The operation is performed by the {@code fragmentManager}.
     *
     */
    public static void replaceFragment (@NonNull FragmentManager fragmentManager,
                                         @NonNull Fragment fragment, int frameId, String tag, boolean backstack) {

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment, tag);
        if (backstack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
        fragmentManager.executePendingTransactions();
    }

    public static void resetToFragment (@NonNull FragmentManager fragmentManager,
                                        @NonNull Fragment fragment, int frameId, String tag, boolean backstack) {
        fragmentManager.popBackStack(tag, 0);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment, tag);
        if (backstack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
        fragmentManager.executePendingTransactions();
    }

    public static void addFragmentToActivity (@NonNull FragmentManager fragmentManager,
                                              @NonNull Fragment fragment, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(fragment, tag);
        transaction.commit();
    }
}