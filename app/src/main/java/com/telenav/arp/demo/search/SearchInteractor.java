package com.telenav.arp.demo.search;

import android.support.annotation.Nullable;

import com.telenav.arp.demo.search.SearchInteractor.SearchPresenter;
import com.uber.rib.core.Bundle;
import com.uber.rib.core.Interactor;
import com.uber.rib.core.RibInteractor;

import javax.inject.Inject;

@RibInteractor
public class SearchInteractor extends Interactor<SearchPresenter, SearchRouter> {
    @Inject
    SearchInteractor.SearchPresenter presenter;

    @Inject
    SearchViewModel viewModel;

    @Inject
    SearchInteractor.Listener listener;

    @Override
    protected void didBecomeActive(@Nullable Bundle savedInstanceState) {
        super.didBecomeActive(savedInstanceState);
        getRouter().getFragment().inject(viewModel);
    }

    void goBack() {
        listener.close();
    }

    void attachDetails(String address) {
        /// TODO: Do backend query
    }

    /** Presenter interface implemented by this RIB's view. */
    interface SearchPresenter {
    }

    /** Listener interface implemented by parent RIB interactor */
    public interface Listener {
        void close();
    }
}