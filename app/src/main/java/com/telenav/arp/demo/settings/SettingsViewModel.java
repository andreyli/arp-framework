package com.telenav.arp.demo.settings;

import android.support.annotation.NonNull;

import com.telenav.arp.ribs.ViewModel;

public class SettingsViewModel extends ViewModel<SettingsInteractor> {
    public SettingsViewModel(SettingsInteractor interactor) {
        super(interactor);
    }

    public void onBackButtonClicked() {
        getInteractor().goBack();
    }

    @NonNull
    @Override
    public String getTag() {
        return getClass().getName();
    }
}