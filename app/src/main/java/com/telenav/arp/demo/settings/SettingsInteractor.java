package com.telenav.arp.demo.settings;

import android.support.annotation.Nullable;

import com.telenav.arp.demo.settings.SettingsInteractor.SettingsPresenter;
import com.uber.rib.core.Bundle;
import com.uber.rib.core.Interactor;
import com.uber.rib.core.RibInteractor;

import javax.inject.Inject;

@RibInteractor
public class SettingsInteractor extends Interactor<SettingsPresenter, SettingsRouter> {
    @Inject
    SettingsInteractor.SettingsPresenter presenter;

    @Inject
    SettingsInteractor.Listener listener;

    @Inject
    SettingsViewModel viewModel;

    @Override
    protected void didBecomeActive(@Nullable Bundle savedInstanceState) {
        super.didBecomeActive(savedInstanceState);
        getRouter().getFragment().inject(viewModel);
    }

    void goBack() {
        listener.close();
    }

    /** Presenter interface implemented by this RIB's view. */
    interface SettingsPresenter {
    }

    public interface Listener {
        void close();
    }
}
