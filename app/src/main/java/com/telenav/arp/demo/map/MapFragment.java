package com.telenav.arp.demo.map;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.telenav.arp.demo.R;
import com.telenav.arp.demo.map.MapBuilder.MapScope;
import com.telenav.arp.ribs.ViewModel.ViewModelInjector;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

@MapScope
public class MapFragment extends DaggerFragment implements MapInteractor.MapPresenter, ViewModelInjector<MapViewModel> {
    private MapViewModel viewModel;

    @Inject
    public MapFragment() {
        // Requires empty public constructor
    }

    @Override
    public void inject(MapViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.map_fragment, container, false);
        Button searchButton = view.findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.handleSearchButtonClick();
            }
        });

        Button settingsButton = view.findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.handleSettingsButtonClick("secret");
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}