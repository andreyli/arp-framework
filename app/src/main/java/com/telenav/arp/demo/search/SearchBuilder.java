package com.telenav.arp.demo.search;

import com.telenav.arp.demo.search.SearchBuilder.ParentComponent;
import com.telenav.arp.ribs.FragmentRouterInjector;
import com.telenav.arp.ribs.FragmentScope;
import com.telenav.arp.ribs.Navigator;
import com.uber.rib.core.Builder;
import com.uber.rib.core.InteractorBaseComponent;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import dagger.Binds;
import dagger.BindsInstance;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

import static java.lang.annotation.RetentionPolicy.CLASS;

public class SearchBuilder extends Builder<SearchRouter, ParentComponent> {

    public SearchBuilder(SearchBuilder.ParentComponent component) {
        super(component);
    }

    public SearchRouter build(Navigator navigator) {
        SearchInteractor interactor = new SearchInteractor();
        Component component = DaggerSearchBuilder_Component.builder()
                .parentComponent(getDependency())
                .navigator(navigator)
                .interactor(interactor)
                .build();
        return component.searchRouter();
    }

    public interface ParentComponent {
        // Define dependencies required from your parent view model here.
        SearchInteractor.Listener searchListener();
    }

    @dagger.Module
    public abstract static class Module {

        @SearchScope
        @Binds
        abstract SearchInteractor.SearchPresenter presenter(SearchFragment fragment);

        @SearchScope
        @Provides
        static SearchRouter router(Component component,
                                   SearchInteractor interactor) {
            return new SearchRouter(interactor, component);
        }

        @SearchScope
        @Provides
        static SearchViewModel viewModel(SearchInteractor interactor) {
            return new SearchViewModel(interactor);
        }
    }

    @dagger.Module
    public abstract class FragmentModule {

        @FragmentScope
        @ContributesAndroidInjector
        abstract SearchFragment searchFragment();
    }

    @SearchScope
    @dagger.Component(modules = { Module.class, FragmentModule.class },
            dependencies = ParentComponent.class)
    interface Component extends
            InteractorBaseComponent<SearchInteractor>,
            FragmentRouterInjector<SearchRouter>,
            BuilderComponent {

        @dagger.Component.Builder
        interface Builder {

            @BindsInstance
            Builder interactor(SearchInteractor interactor);

            @BindsInstance
            Builder navigator(Navigator navigator);

            Builder parentComponent(ParentComponent component);

            Component build();
        }
    }

    interface BuilderComponent {
        SearchRouter searchRouter();
    }

    @Scope
    @Retention(CLASS)
    @interface SearchScope {}
}

/*    private static final class SearchFragmentFactory extends FragmentFactory {
        SearchFragmentFactory(Navigator navigator) {
            super(navigator);
        }

        @Override
        protected Fragment findOrCreateFragment(ViewModel viewModel) {
            if (viewModel instanceof SearchViewModel) {
                SearchFragment fragment = (SearchFragment)findFragment(SearchFragment.class);
                if (fragment == null) {
                    fragment = SearchFragment.newInstance();
                }
                fragment.inject((SearchViewModel) viewModel);
                return fragment;
            } else if (viewModel instanceof SearchDetailsViewModel) {
                SearchDetailsFragment fragment = (SearchDetailsFragment)findFragment(SearchDetailsFragment.class);
                if (fragment == null) {
                    fragment = SearchDetailsFragment.newInstance();
                }
                fragment.inject((SearchDetailsViewModel) viewModel);
                return fragment;
            }
            throw new IllegalArgumentException("Unknown View Model class: " + viewModel.getClass().getName());
        }
    }

    private static final class SearchViewModelFactory extends RetainedViewModelFactory<SearchFlow> {
        private Dependency dependency;

        SearchViewModelFactory(Navigator navigator, Dependency dependency) {
            super(navigator);
            this.dependency = dependency;
        }

        @Override
        protected ViewModel<SearchFlow> findOrCreateViewModel(Class<? extends ViewModel> clazz, SearchFlow flow) {
            ViewModel viewModel = findViewModel(clazz);
            if (viewModel != null) {
                return viewModel;
            }

            if (clazz.isAssignableFrom(SearchViewModel.class)) {
                return new SearchViewModel(flow, dependency.listener());
            } else if (clazz.isAssignableFrom(SearchDetailsViewModel.class)) {
                return new SearchDetailsViewModel(flow, dependency.navigateTo());
            } else {
                throw new IllegalArgumentException("Unknown Fragment class: " + clazz.getName());
            }
        }
    } */