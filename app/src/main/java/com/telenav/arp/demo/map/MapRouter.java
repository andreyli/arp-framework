package com.telenav.arp.demo.map;

import android.support.annotation.Nullable;

import com.telenav.arp.demo.map.MapBuilder.Component;
import com.telenav.arp.demo.search.SearchBuilder;
import com.telenav.arp.demo.search.SearchRouter;
import com.telenav.arp.demo.settings.SettingsBuilder;
import com.telenav.arp.demo.settings.SettingsRouter;
import com.telenav.arp.ribs.FragmentRouter;

import javax.inject.Inject;

import dagger.Lazy;

public class MapRouter extends FragmentRouter<MapFragment, MapInteractor, Component> {
    private final SearchBuilder searchBuilder;
    private final SettingsBuilder settingsBuilder;

    @Nullable
    private SearchRouter searchRouter;
    private SettingsRouter settingsRouter;

    @Inject
    Lazy<MapFragment> mapFragmentProvider;

    public MapRouter(MapInteractor interactor,
                     MapBuilder.Component component,
                     SearchBuilder searchBuilder,
                     SettingsBuilder settingsBuilder) {
        super(interactor, component);
        this.searchBuilder = searchBuilder;
        this.settingsBuilder = settingsBuilder;
    }

    @Override
    public MapFragment getFragment() {
        return mapFragmentProvider.get();
    }

    void attachSearch() {
        searchRouter = searchBuilder.build(getNavigator());
        attachChild(searchRouter);
        getNavigator().goTo(searchRouter.getFragment());
    }

    void detachSearch() {
        if (searchRouter != null) {
            detachChild(searchRouter);
            searchRouter = null;
        }
        getNavigator().goBack();
    }

    void attachSettings() {
        settingsRouter = settingsBuilder.build(getNavigator());
        attachChild(settingsRouter);
        getNavigator().goTo(settingsRouter.getFragment());
    }

    void detachSettings() {
        if (settingsRouter != null) {
            detachChild(settingsRouter);
            settingsRouter = null;
        }
        getNavigator().goBack();
    }
}