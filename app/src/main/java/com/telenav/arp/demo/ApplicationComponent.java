package com.telenav.arp.demo;

import android.app.Application;

import com.telenav.arp.ribs.ApplicationModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = { ApplicationModule.class,
                       RootActivityBindingModule.class,
                       AndroidSupportInjectionModule.class })
public interface ApplicationComponent extends AndroidInjector<ARPApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        ApplicationComponent.Builder application(Application application);

        ApplicationComponent build();
    }
}