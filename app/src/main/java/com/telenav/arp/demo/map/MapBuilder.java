package com.telenav.arp.demo.map;

import com.telenav.arp.demo.map.MapBuilder.ParentComponent;
import com.telenav.arp.demo.search.SearchBuilder;
import com.telenav.arp.demo.search.SearchInteractor;
import com.telenav.arp.demo.settings.SettingsBuilder;
import com.telenav.arp.demo.settings.SettingsInteractor;
import com.telenav.arp.ribs.FragmentRouterInjector;
import com.telenav.arp.ribs.FragmentScope;
import com.telenav.arp.ribs.Navigator;
import com.uber.rib.core.Builder;
import com.uber.rib.core.InteractorBaseComponent;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import dagger.Binds;
import dagger.BindsInstance;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

import static java.lang.annotation.RetentionPolicy.CLASS;

public class MapBuilder extends Builder<MapRouter, ParentComponent> {
    public MapBuilder(ParentComponent dependency) {
        super(dependency);
    }

    public MapRouter build(Navigator navigator) {
        MapInteractor interactor = new MapInteractor();
        Component component = DaggerMapBuilder_Component.builder()
                .parentComponent(getDependency())
                .navigator(navigator)
                .interactor(interactor)
                .build();
        return component.mapRouter();
    }

    public interface ParentComponent {
        // Define dependencies required from your parent view model here.
    }

    @dagger.Module
    public abstract static class Module {

        @MapScope
        @Binds
        abstract MapInteractor.MapPresenter presenter(MapFragment fragment);

        @MapScope
        @Provides
        static MapRouter router(Component component, MapInteractor interactor) {
            return new MapRouter(interactor, component,
                    new SearchBuilder(component),
                    new SettingsBuilder(component));
        }

        @MapScope
        @Provides
        static MapViewModel viewModel(MapInteractor interactor) {
            return new MapViewModel(interactor);
        }

        @MapScope
        @Provides
        static SearchInteractor.Listener searchListener(MapInteractor interactor) {
            return interactor.new SearchListener();
        }

        @MapScope
        @Provides
        static SettingsInteractor.Listener settingsListener(MapInteractor interactor) {
            return interactor.new SettingsListener();
        }
    }

    @dagger.Module
    public abstract class FragmentModule {

        @FragmentScope
        @ContributesAndroidInjector
        abstract MapFragment mapFragment();
    }


    @MapScope
    @dagger.Component(modules = { Module.class, FragmentModule.class },
            dependencies = ParentComponent.class)
    interface Component extends
            InteractorBaseComponent<MapInteractor>,
            FragmentRouterInjector<MapRouter>,
            BuilderComponent,
            SearchBuilder.ParentComponent,
            SettingsBuilder.ParentComponent {

        @dagger.Component.Builder
        interface Builder {

            @BindsInstance
            Builder interactor(MapInteractor interactor);

            @BindsInstance
            Builder navigator(Navigator navigator);

            Builder parentComponent(ParentComponent component);

            Component build();
        }
    }

    interface BuilderComponent {
        MapRouter mapRouter();
    }

    @Scope
    @Retention(CLASS)
    @interface MapScope {}
}
/*
    private static final class RootViewFactory extends FragmentFactory {
        RootViewFactory(Navigator navigator) {
            super(navigator);
        }

        @Override
        protected Fragment findOrCreateFragment(ViewModel viewModel) {
            if (viewModel.getTag().equals(MapViewModel.class.getName())) {
                MapFragment fragment = (MapFragment)findFragment(MapFragment.class);
                if (fragment == null) {
                    fragment = MapFragment.newInstance();
                }
                fragment.inject((MapViewModel) viewModel);
                return fragment;
            }
            throw new IllegalArgumentException("Unknown View Model class: " + viewModel.getClass().getName());
        }
    }

    private static final class RootViewModelFactory extends RetainedViewModelFactory<RootFlow> {
        private SearchViewModel.Listener searchListener;
        private SettingsViewModel.Listener settingsListener;
        private SearchDetailsViewModel.Listener searchDetailsListener;

        RootViewModelFactory(Navigator navigator) {
            super(navigator);
        }

        SearchViewModel.Listener getSearchListener() {
            return searchListener;
        }

        SettingsViewModel.Listener getSettingsListener() {
            return settingsListener;
        }

        SearchDetailsViewModel.Listener getSearchDetailsListener() { return searchDetailsListener; }

        @Override
        protected ViewModel<RootFlow> findOrCreateViewModel(Class<? extends ViewModel> clazz, RootFlow flow) {
            MapViewModel viewModel = (MapViewModel)findViewModel(clazz);
            if (viewModel != null) {
                return viewModel;
            }

            if (clazz.isAssignableFrom(MapViewModel.class)) {
                viewModel = new MapViewModel(flow);
                searchListener = viewModel.new SearchHandler();
                settingsListener = viewModel.new SettingsHandler();
                searchDetailsListener = viewModel.new SearchDetailsHandler();
                return viewModel;
            } else {
                throw new IllegalArgumentException("Unknown View Model class: " + clazz.getName());
            }
        }
    } */