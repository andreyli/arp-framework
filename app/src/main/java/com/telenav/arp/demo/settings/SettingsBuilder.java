package com.telenav.arp.demo.settings;

import com.telenav.arp.demo.settings.SettingsBuilder.ParentComponent;
import com.telenav.arp.ribs.FragmentRouterInjector;
import com.telenav.arp.ribs.FragmentScope;
import com.telenav.arp.ribs.Navigator;
import com.uber.rib.core.Builder;
import com.uber.rib.core.InteractorBaseComponent;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import dagger.Binds;
import dagger.BindsInstance;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

import static java.lang.annotation.RetentionPolicy.CLASS;

public class SettingsBuilder extends Builder<SettingsRouter, ParentComponent> {
    public SettingsBuilder(ParentComponent component) {
        super(component);
    }

    public SettingsRouter build(Navigator navigator) {
        SettingsInteractor interactor = new SettingsInteractor();
        Component component = DaggerSettingsBuilder_Component.builder()
                .parentComponent(getDependency())
                .navigator(navigator)
                .interactor(interactor)
                .build();
        return component.settingsRouter();
    }

    public interface ParentComponent {
        // Define dependencies required from your parent view model here.
        SettingsInteractor.Listener settingsListener();
    }

    @dagger.Module
    public abstract static class Module {

        @SettingsScope
        @Binds
        abstract SettingsInteractor.SettingsPresenter presenter(SettingsFragment fragment);

        @SettingsScope
        @Provides
        static SettingsRouter router(Component component, SettingsInteractor interactor) {
            return new SettingsRouter(interactor, component);
        }

        @SettingsScope
        @Provides
        static SettingsViewModel viewModel(SettingsInteractor interactor) {
            return new SettingsViewModel(interactor);
        }
    }

    @dagger.Module
    public abstract class FragmentModule {

        @FragmentScope
        @ContributesAndroidInjector
        abstract SettingsFragment settingsFragment();
    }

    @SettingsScope
    @dagger.Component(modules = { Module.class, FragmentModule.class },
            dependencies = ParentComponent.class)
    interface Component extends
            InteractorBaseComponent<SettingsInteractor>,
            FragmentRouterInjector<SettingsRouter>,
            BuilderComponent {

        @dagger.Component.Builder
        interface Builder {

            @BindsInstance
            Builder interactor(SettingsInteractor interactor);

            @BindsInstance
            Builder navigator(Navigator navigator);

            Builder parentComponent(ParentComponent component);

            Component build();
        }
    }

    interface BuilderComponent {
        SettingsRouter settingsRouter();
    }

    @Scope
    @Retention(CLASS)
    @interface SettingsScope {
    }
}

/*    private static final class SettingsFragmentFactory extends FragmentFactory {
        SettingsFragmentFactory(Navigator navigator) {
            super(navigator);
        }

        @Override
        protected Fragment findOrCreateFragment(ViewModel viewModel) {
            if (viewModel instanceof SettingsViewModel) {
                SettingsFragment fragment = (SettingsFragment)findFragment(SettingsFragment.class);
                if (fragment == null) {
                    fragment = SettingsFragment.newInstance();
                }
                fragment.inject((SettingsViewModel) viewModel);
                return fragment;
            }
            throw new IllegalArgumentException("Unknown View Model class: " + viewModel.getClass().getName());
        }
    }

    private static final class SettingsViewModelFactory extends RetainedViewModelFactory<SettingsFlow> {
        private String param;
        private ParentFeature dependency;

        SettingsViewModelFactory(Navigator navigator, ParentFeature dependency, String param) {
            super(navigator);
            this.dependency = dependency;
            this.param = param;
        }

        @Override
        protected ViewModel<SettingsFlow> findOrCreateViewModel(Class<? extends ViewModel> clazz, SettingsFlow flow) {
            SettingsViewModel viewModel = (SettingsViewModel)findViewModel(clazz);
            if (viewModel != null) {
                return viewModel;
            }

            if (clazz.isAssignableFrom(SettingsViewModel.class)) {
                return new SettingsViewModel(flow, param, dependency.listener());
            } else {
                throw new IllegalArgumentException("Unknown Fragment class: " + clazz.getName());
            }
        }
    }
*/