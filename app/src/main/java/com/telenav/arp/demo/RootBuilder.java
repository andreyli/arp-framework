package com.telenav.arp.demo;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.telenav.arp.demo.map.MapBuilder;
import com.telenav.arp.ribs.Navigator;
import com.uber.rib.core.InteractorBaseComponent;
import com.uber.rib.core.ViewBuilder;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import dagger.Binds;
import dagger.BindsInstance;
import dagger.Provides;

import static java.lang.annotation.RetentionPolicy.CLASS;

public class RootBuilder extends ViewBuilder<RootView, RootRouter, RootBuilder.ParentComponent> {
    public RootBuilder(ParentComponent dependency) {
        super(dependency);
    }

    /**
     * Builder assembles dependencies
     * @return Root router
     */
    public RootRouter build(Navigator navigator, ViewGroup parentViewGroup) {
        RootView rootView = createView(parentViewGroup);
        RootInteractor interactor = new RootInteractor();
        Component component = DaggerRootBuilder_Component.builder()
                .parentComponent(getDependency())
                .view(rootView)
                .navigator(navigator)
                .interactor(interactor)
                .build();
        return component.rootRouter();
    }

    @Override
    protected RootView inflateView(LayoutInflater inflater, ViewGroup parentViewGroup) {
        return (RootView) inflater.inflate(R.layout.root_fragment, parentViewGroup, false);
    }

    public interface ParentComponent {
        // Define dependencies required from your parent view model here.
    }

    @dagger.Module
    public abstract static class Module {

        @RootScope
        @Binds
        abstract RootInteractor.RootPresenter presenter(RootView view);

        @RootScope
        @Provides
        static RootRouter router(Component component, RootView view, RootInteractor interactor,
                                 Navigator navigator) {
            return new RootRouter(navigator, view, interactor, component,
                    new MapBuilder(component));
        }
    }

    @RootScope
    @dagger.Component(
            modules = Module.class,
            dependencies = ParentComponent.class
    )
    interface Component extends
            InteractorBaseComponent<RootInteractor>,
            BuilderComponent,
            MapBuilder.ParentComponent {

        @dagger.Component.Builder
        interface Builder {
            @BindsInstance
            Builder interactor(RootInteractor interactor);

            @BindsInstance
            Builder view(RootView view);

            Builder parentComponent(ParentComponent component);

            @BindsInstance
            Builder navigator(Navigator navigator);

            Component build();
        }
    }

    interface BuilderComponent {
        RootRouter rootRouter();
    }

    @Scope
    @Retention(CLASS)
    @interface RootScope {}
}