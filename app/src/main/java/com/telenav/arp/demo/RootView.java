package com.telenav.arp.demo;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.uber.rib.core.Initializer;

public class RootView extends FrameLayout implements RootInteractor.RootPresenter {
    public RootView(Context context) {
        this(context, null);
    }

    public RootView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RootView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Initializer
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }
}
