package com.telenav.arp.demo;

import com.telenav.arp.demo.map.MapBuilder;
import com.telenav.arp.demo.search.SearchBuilder;
import com.telenav.arp.demo.settings.SettingsBuilder;
import com.telenav.arp.ribs.ActivityScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class RootActivityBindingModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = { MapBuilder.FragmentModule.class,
                                            SearchBuilder.FragmentModule.class,
                                            SettingsBuilder.FragmentModule.class
    })
    abstract RootActivity rootActivity();
}