package com.telenav.arp.demo.search;

import android.support.annotation.NonNull;

import com.telenav.arp.ribs.ViewModel;

public class SearchDetailsViewModel extends ViewModel<SearchInteractor> {
    public SearchDetailsViewModel(SearchInteractor interactor) {
        super(interactor);
    }

    public void onBackButtonClicked() {
        // This is a very simplistic case just go back to previous screen
        // No data needs to be passed back to previous state
        getInteractor().goBack();
    }

    public void navigateTo(String destination) {
    }

    @NonNull
    @Override
    public String getTag() {
        return getClass().getName();
    }
}
