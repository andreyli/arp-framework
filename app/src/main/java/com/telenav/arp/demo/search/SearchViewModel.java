package com.telenav.arp.demo.search;

import android.support.annotation.NonNull;

import com.telenav.arp.ribs.ViewModel;

public class SearchViewModel extends ViewModel<SearchInteractor> {
    public SearchViewModel(SearchInteractor interactor) {
        super(interactor);
    }

    public void onGoButtonClicked(String destinationAddress) {
        getInteractor().attachDetails(destinationAddress);
    }

    public void onBackButtonClicked() {
        getInteractor().goBack();
    }

    @NonNull
    @Override
    public String getTag() {
        return getClass().getName();
    }
}
