package com.telenav.arp.demo.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.telenav.arp.demo.R;
import com.telenav.arp.demo.search.SearchBuilder.SearchScope;
import com.telenav.arp.ribs.ViewModel.ViewModelInjector;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

@SearchScope
public class SearchFragment extends DaggerFragment implements SearchInteractor.SearchPresenter, ViewModelInjector<SearchViewModel> {
    private SearchViewModel viewModel;
    private EditText searchText;

    @Inject
    public SearchFragment() {
        // Requires empty public constructor
    }

    @Override
    public void inject(SearchViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment, container, false);

        searchText = view.findViewById(R.id.searchEditText);

        Button backButton = view.findViewById(R.id.backButton);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.onBackButtonClicked();
            }
        });

        Button goButton = view.findViewById(R.id.goButton);
        goButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.onGoButtonClicked(searchText.getText().toString());
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}